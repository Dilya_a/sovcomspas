// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ СТАРТ
		
		// $(function(){
		// 	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
		// 	ua = navigator.userAgent,

		// 	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

		// 	scaleFix = function () {
		// 		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
		// 			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
		// 			document.addEventListener("gesturestart", gestureStart, false);
		// 		}
		// 	};
			
		// 	scaleFix();
		// });
		// var ua=navigator.userAgent.toLocaleLowerCase(),
		//  regV = /ipod|ipad|iphone/gi,
		//  result = ua.match(regV),
		//  userScale="";
		// if(!result){
		//  userScale=",user-scalable=0"
		// }
		// document.write('<meta name="viewport" id="myViewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

		// ============================================================
		//  window.onload = function () {
		// 	if(screen.width <= 617) {
		// 	    var mvp = document.getElementById('myViewport');
		// 	    mvp.setAttribute('content','width=617');
		// 	}
		// }
		// ============================================================

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ КОНЕЦ




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/




		var sticky = $(".sticky"),
			tabs = $('.tabs'),
		    styler = $(".styler"),
		    diagram = $("#diagram"),
			windowW = $(window).width(),
			windowH = $(window).height();


			if(sticky.length){
					include("plugins/sticky.js");
					include("plugins/jquery.smoothscroll.js");
			}
			if(styler.length){
					include("plugins/formstyler/formstyler.js");
			}
			if(tabs.length){
					include("plugins/easy-responsive-tabs/easyResponsiveTabs.js");
			}
			if(diagram.length){
					include('plugins/chart.min.js');
			}

					include("plugins/modernizr.js");



			function include(url){ 

					document.write('<script src="'+ url + '"></script>'); 

			}

		


		$(document).ready(function(){




			/* ------------------------------------------------
			STICKY START
			------------------------------------------------ */

					if (sticky.length){
						$(sticky).sticky({
					        topspacing: 0,
					        styler: 'is-sticky',
					        animduration: 0,
					        unlockwidth: false,
					        screenlimit: false,
					        sticktype: 'alonemenu'
						});
					};

			/* ------------------------------------------------
			STICKY END
			------------------------------------------------ */




			/* ------------------------------------------------
			FORMSTYLER START
			------------------------------------------------ */

					if (styler.length){
						styler.styler({
							// selectSmartPositioning: true
						});
					}

			/* ------------------------------------------------
			FORMSTYLER END
			------------------------------------------------ */



			/* ------------------------------------------------
			TABS START
			------------------------------------------------ */

					if(tabs.length){
						tabs.easyResponsiveTabs();
					}

			/* ------------------------------------------------
			TABS END
			------------------------------------------------ */


			/* ------------------------------------------------
			ACCORDION START
			------------------------------------------------ */

					if($('.accordion_item_link').length){
						$('.accordion_item_link').on('click', function(){
							$(this)
							.toggleClass('active')
							.next('.sub-menu')
							.slideToggle()
							.parents(".accordion_item")
							.siblings(".accordion_item")
							.find(".accordion_item_link")
							.removeClass("active")
							.next(".sub-menu")
							.slideUp();
						});  
					}

			/* ------------------------------------------------
			ACCORDION END
			------------------------------------------------ */

			/* ------------------------------------------------
			DIAGRAM START
			------------------------------------------------ */

					if(diagram.length){

						var countries= document.getElementById("diagram").getContext("2d");
	                    var pieData = [
	                       {
	                          value : diagram.parent().data('interest-orange'),
	                          color : "#ff4800"
	                       },
	                       {
	                          value: diagram.parent().data('interest-blue'),
	                          color:"#005376"
	                       }
	                    ];

	                    var pieOptions = {
	                        segmentShowStroke : true,
	                        animateScale : true,
	                        segmentStrokeWidth: 0
	                    }

	                    new Chart(countries).Pie(pieData, pieOptions);
	                }    

			/* ------------------------------------------------
			DIAGRAM END
			------------------------------------------------ */




		});




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================

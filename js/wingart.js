

$(document).ready(function(){




		/* ------------------------------------------------
		ANCHOR START
		------------------------------------------------ */
				 
			    function goUp(){
					var windowHeight = $(window).height(),
						windowScroll = $(window).scrollTop();

					if(windowScroll>windowHeight/2){
						$('.arrow_up').addClass('active');
					}

					else{
						$('.arrow_up').removeClass('active');
					}

			    }

			    goUp();
				$(window).on('scroll',goUp);

				$('.arrow_up').on('click ontouchstart',function () {

					if($.browser.safari){
						$('body').animate( { scrollTop: 0 }, 1100 );
					}
					else{
						$('html,body').animate( { scrollTop: 0}, 1100 );
					}
					return false;
					
				});

		/* ------------------------------------------------
		ANCHOR END
		------------------------------------------------ */



		/* ------------------------------------------------
		RESPONSIVE MENU START
		------------------------------------------------ */

				$(".resp_btn, .close_resp_menu, show_tab_btn").on("click ontouchstart", function(){
					$("body").toggleClass("show_menu")
				});

				$(document).on("click ontouchstart", function(event) {
			      if ($(event.target).closest("nav,.resp_btn").length) return;
			      $("body").removeClass("show_menu");
			      if(windowW <= 991){
			      	$(".menu_item").removeClass("active").find(".dropdown-menu").css("display","none");
			      }
			      event.stopPropagation();
			    });

				// проверка на наличие элемента и вставка хтмл кода
			 	//  if($(window).width() <= 767){
				// 	if ($(".menu_item").length){
				//         $(".menu_item").has('.dropdown-menu').append('<span class="touch-button"><i class="fa fa-angle-down"></i></span>');
				//     }
				// }
				// проверка на наличие элемента и вставка хтмл кода


				$('.menu_link').on('click ontouchstart',function(event){
					if($("html").hasClass("md_no-touch"))return;

			        var windowWidth = $(window).width(),
			            $parent = $(this).parent('.menu_item');
			        if(windowWidth > 991){
			          // if($("html").hasClass("md_touch")){
			            if((!$parent.hasClass('active')) && $parent.find('.dropdown-menu').length){

			              event.preventDefault();

			              $parent.toggleClass('active')
			               .siblings()
			               .find('.menu_link')
			               .removeClass('active');
			            }
			          // }  
			        }
			        
			        else{
			            
			          if((!$parent.hasClass('active')) && $parent.find('.dropdown-menu').length){

			            event.preventDefault();

			            $parent.toggleClass('active')
			             .siblings()
			             .removeClass('active');
			            $parent.find(".dropdown-menu")
			             .slideToggle()
			             .parents('.menu_item')
			             .siblings()
			             .find(".dropdown-menu")
			             .slideUp();
			          }
			        }

			    });

		/* ------------------------------------------------
		RESPONSIVE MENU END
		------------------------------------------------ */


			$(".show_tab_btn").on("click", function(){
			    $("body").toggleClass("show_tab_menu");
			});

		

		
});